import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';

const emailInput = document.getElementById('Email');
const passwordInput = document.getElementById('Password');
const loginButton = document.querySelector('#btnEnviar');
const errorMessage = document.getElementById('error-message');

console.log(loginButton);
loginButton.addEventListener('click', async (e) => {
  e.preventDefault();

  const email = emailInput.value;
  const password = passwordInput.value;

  const auth = getAuth();

  try {
    await signInWithEmailAndPassword(auth, email, password);
    window.location.href = '/html/database.html';
  } catch (error) {
    let errorMessage = "llene todos los campos o Contraseña incorrecta."
    if(error.code === 'auth/wrong-password'){
      errorMessage= 'La contraseña es incorrecta. Intente Nuevamente';
      
    }else if (error.errorMessage){
      errorMessage = error.errorMessage;
    }
    alert(errorMessage);
  }
});