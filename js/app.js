import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js"; 


const firebaseConfig = {
  apiKey: "AIzaSyARnPGStRx0LJMzorZvrLpKzCJZJpOmYqA",
  authDomain: "administrador-web-ebe21.firebaseapp.com",
  projectId: "administrador-web-ebe21",
  storageBucket: "administrador-web-ebe21.appspot.com",
  messagingSenderId: "356444582693",
  appId: "1:356444582693:web:913ad6fe14ad1cea9f60cc"
};

const app = initializeApp(firebaseConfig);
export {app}
export const auth = getAuth(app);
export const storage = getStorage(app);
export const database = getDatabase(app);

  